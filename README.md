# huy-fidel-330e

## CS330e
Elements of Software Engineering I

## Description
This project contains projects/assignments related to the class CS330e with Professor Fares Fraij

## Support
For help, please go to [this link](https://www.cs.utexas.edu/~fares/cs330ef23/list.html).

## Roadmap
TBD

## Contributing
Thanks to Fidel and Huy for contributing to this repository